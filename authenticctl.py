#! /usr/bin/env python

import sys

def print_usage():
    print 'Usage: authenticctl.py command [...]'
    print ''
    print 'Commands:'
    print '  start                start server'
    print '  clean_sessions       clean old sessions'
    print '  lock                 lock/unlock things'


if len(sys.argv) < 2:
    print_usage()
    sys.exit(1)
else:
    command = sys.argv[1]

    if command == 'start':
        from authentic.ctl.start import start
        start(sys.argv[2:])
    elif command == 'clean_sessions':
        from authentic.ctl.clean_sessions import clean_sessions
        clean_sessions(sys.argv[2:])
    elif command == 'lock':
        from authentic.ctl.lock import lock
        lock(sys.argv[2:])
    else:
        print_usage()

