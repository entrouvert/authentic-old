import cStringIO
import os
import re
import tempfile

from quixote import get_publisher, get_request, get_session
from quixote.http_request import Upload
from qommon.form import *
from quixote.html import htmltext, TemplateIO
from quixote.util import randbytes

class ValidatedStringWidget(StringWidget):
    def __init__(self, *args, **kwargs):
        self.regex = kwargs.pop('regex', None)
        StringWidget.__init__(self, *args, **kwargs)

    def _parse(self, request):
        StringWidget._parse(self, request)
        if self.regex and self.value is not None:
            if not re.match(self.regex, self.value):
                self.error = _('wrong format')
