from cStringIO import StringIO
import traceback

import quixote

DEFAULT_CHARSET = 'iso-8859-1'

from Defaults import *

try:
    from authentic_cfg import *
except ImportError:
    pass

from qommon.publisher import get_publisher_class, set_publisher_class, QommonPublisher

from root import RootDirectory
from admin.root import RootDirectory as AdminRootDirectory
import sessions

import identities

quixote.DEFAULT_CHARSET = DEFAULT_CHARSET

class AuthenticPublisher(QommonPublisher):
    APP_NAME = 'authentic'
    APP_DIR = APP_DIR
    DATA_DIR = DATA_DIR
    ERROR_LOG = ERROR_LOG
    WEBROOT_DIR = None

    supported_languages = ['fr', 'lv', 'ro', 'it', 'es']
    # to modify charset of an v
    site_charset = 'utf-8'

    root_directory_class = RootDirectory
    admin_directory_class = AdminRootDirectory

    session_manager_class = sessions.StorageSessionManager

    def register_ident_methods(self):
        pass

    def set_config(self, request = None):
        QommonPublisher.set_config(self, request = request)
        # Overload the cookie name
        if self.cfg.get('cookie_name'):
            self.config.session_cookie_name = self.cfg['cookie_name']
        self.store = identities.load_store()

    def _generate_plaintext_error(self, request, original_response,
                                  exc_type, exc_value, tb):
        # just like the parent one, but add lasso identity and session dumps
        # to message.
        error_file = StringIO()

        # be wise, use accumulated experience.
        error_file.write(QommonPublisher._generate_plaintext_error(self, request, original_response, exc_type, exc_value, tb))

        # add Lasso stuff
        if request and request.session:
            session = request.session
            if session.lasso_session_dump:
                error_file.write('\n\nLasso Session Dump:\n')
                error_file.write(session.lasso_session_dump)
                error_file.write('\n')

            import authentic.identities
            try:
                identity = authentic.identities.get_store().get_identity(session.user)
            except KeyError:
                pass
            else:
                if identity.lasso_dump:
                    error_file.write('\n\nLasso Identity Dump:\n')
                    error_file.write(identity.lasso_dump)
                    error_file.write('\n')
                if identity.lasso_proxy_dump:
                    error_file.write('\n\nLasso Identity Proxy Dump:\n')
                    error_file.write(identity.lasso_proxy_dump)
                    error_file.write('\n')

            if session.lasso_proxy_session_dump:
                error_file.write('\n\nLasso Proxy Session Dump:\n')
                error_file.write(session.lasso_proxy_session_dump)
                error_file.write('\n')

            if session.lasso_login_dump:
                error_file.write('\n\nLasso Login Dump:\n')
                error_file.write(session.lasso_login_dump)
                error_file.write('\n')

            if session.lasso_logout_dump:
                error_file.write('\n\nLasso Logout Dump:\n')
                error_file.write(session.lasso_logout_dump)
                error_file.write('\n')

        return error_file.getvalue()

set_publisher_class(AuthenticPublisher)

