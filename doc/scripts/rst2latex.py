#! /usr/bin/python

"""A minimal reST frontend, to create appropriate LaTeX files."""

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

from docutils.core import publish_cmdline, Publisher

def set_io(self, source_path=None, destination_path=None):
    Publisher.set_io_orig(self, source_path, destination_path='/dev/null')

Publisher.set_io_orig, Publisher.set_io = Publisher.set_io, set_io

output = publish_cmdline(writer_name='latex',
    settings_overrides = {
        'documentclass': 'report',
        'documentoptions': '11pt,a4paper,titlepage',
        'use_latex_toc': True,
        'use_latex_docinfo': True,
        'stylesheet': 'custom.tex'})

output = output.replace('\\includegraphics',
    '\\includegraphics[width=.9\\textwidth,height=15cm,clip,keepaspectratio]')
output = output.replace('\\begin{figure}[htbp]', '\\begin{figure}[H]')
print output
