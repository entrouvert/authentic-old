#! /usr/bin/env python

import os
import distutils.core
from quixote.ptl.qx_distutils import qx_build_py


local_cfg = None
if os.path.exists('authentic/authentic_cfg.py'):
    local_cfg = file('authentic/authentic_cfg.py').read()
    os.unlink('authentic/authentic_cfg.py')

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js', '.ezt']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append( (root.replace(sourcedir, destdir, 1), l) )
        if '.svn' in dirs:
            dirs.remove('.svn')
    return r

distutils.core.setup(
        name = 'authentic',
        version = '0.7.9',
        maintainer = 'Frederic Peters',
        maintainer_email = 'fpeters@entrouvert.com',
        url = 'http://authentic.labs.libre-entreprise.org',
        package_dir = { 'authentic': 'authentic' },
        packages = ['authentic', 'authentic.admin', 'authentic.liberty', 'authentic.ctl',
                'authentic.external.qommon', 'authentic.external.qommon.admin',
                'authentic.external.qommon.backoffice', 'authentic.external.qommon.ident',
                'authentic.external.qommon.vendor', 'authentic.schemas'],
        cmdclass = {'build_py': qx_build_py},
        scripts = ['authenticctl.py'],
        data_files = data_tree('share/authentic/', 'data/')
    )

if local_cfg:
    file('authentic/authentic_cfg.py', 'w').write(local_cfg)
