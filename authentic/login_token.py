# w.c.s. - web application for online forms
# Copyright (C) 2005-2010  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301  USA

import random
import string
import datetime

from qommon.storage import StorableObject
import time

class LoginToken(StorableObject):
    '''Keep the result of an authentication transaction'''
    _names = "login_token"
    authentication = None

    def __init__(self, id, expiration_delay = 120):
        StorableObject.__init__(self, id)
        self.expiration = time.time() + expiration_delay

    def valid(self):
        return self.expiration >= time.time()

    def set_authentication_result(cls, id, result, user = None,
            instant = datetime.datetime.utcnow(), method = ''):
        if id:
            tok = cls.get(id, ignore_errors=True)
            if tok:
                tok.authentication = result
                tok.authentication_instant = datetime.datetime.utcnow()
                tok.authentication_method = method
                tok.user = user
                tok.store()
    set_authentication_result = classmethod(set_authentication_result)

    def has_good_authentication(cls, id):
        tok = cls.get(id, ignore_errors=True)
        if tok is None:
            return False
        return tok.authentication is True
    has_good_authentication = classmethod(has_good_authentication)

    def get(cls, id, ignore_errors=False, ignore_migration=False):
        o = super(LoginToken, cls).get(id, ignore_errors, ignore_migration)
        if o and not o.valid():
            o.remove_self()
            if ignore_errors:
                return None
            raise KeyError('LoginToken is invalid, automatically removed!')
        return o
    get = classmethod(get)
