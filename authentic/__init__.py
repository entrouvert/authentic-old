import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'external'))

import qommon
import identities
import liberty.root
import liberty.saml2

# Retro-compatibility with old module paths
sys.modules['identities'] = identities
sys.modules['liberty.root'] = liberty.root
sys.modules['liberty.saml2'] = liberty.saml2
