=====================================
Authentic - Guide de l'administrateur
=====================================

:auteur: Pierre Cros
:contact: pcros@entrouvert.com
:copyright: Copyright © 2005 Entr'ouvert

.. contents:: Table des matières
.. section-numbering::

Vue générale
============

Authentic est une solution de gestion des identités (un fournisseur d'identité)
permettant les fédérations et l'utilisation du Single Sign-On en respectant les
normes et standards définis par le projet `Liberty Alliance`_ (ID-FF 1.2 et
ID-WSF). Il utilise la librairie Lasso_ certifiée conforme par `Liberty
Alliance`_. Lasso_ et Authentic sont publiés sous la `licence GNU/GPL`_.

Qu'est-ce que la fédération d'identité ?
++++++++++++++++++++++++++++++++++++++++

La fédération d'identité est un concept né du rapprochement de besoins
commerciaux et de technologies permettant d'élaborer des échanges entre réseaux
et domaines différents de façon sécurisée et fiable.

La fédération vise principalement le partage d'information d'identification
entre systèmes et plates-formes d'identification hétérogènes.

Un système basé sur une identité fédérée permets aux utilisateurs de se
connecter en utilisant un seul identifiant et mot de passe  (ou un autre moyen
d'authentification) au lieu d'en avoir un pour chaque service. Cet identifiant
et ce mot de passe sont saisis une seule fois lors de la connexion au premier
service. L'utilisateur est ensuite automatiquement authentifié sur tous les
services  partageant l'identité fédérée.

On peut comparer une identité fédérée à un passeport utilisé pour prouver notre
identité et nous permettre de voyager d'un pays à l'autre.

Qu'apporte la fédération d'identité ?
+++++++++++++++++++++++++++++++++++++

Les apports peuvent être nombreux :

* sécuriser les accès à vos applications sur tous les réseaux, publics et
  privés (et tout particulièrement concernant des transactions extranet que l'on
  peut étendre) ;

* simplifier l'accès à vos applications par l'utilisation des technologies
  Single Sign-On (simplification pour les utilisateurs et pour les
  administrateurs) ;

* réduire les coûts (coûts de helpdesk en particulier, coûts liés à la gestion
  des utilisateurs d'un partenaire ou d'un client) ;

* certains protocoles de fédération permettent en plus de garantir le respect
  de la vie privée de l'utilisateur (contrôle de l'utilisateur sur les données
  échangées, pas d'identifiant unique en circulation).


Les différents protocoles et standards de la fédération d'identité
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ils sont nombreux et souvent liés. Au fil du temps, de nouveaux standards ont
émergé pour répondre à de nouveau besoins. SAML_ (Security Assertion Markup
Language) est par exemple la base commune d'ID-FF (Identity Federation
Framework de `Liberty Alliance`_) et de Shibboleth_.

Par ailleurs une nouvelle couche est venue se greffer sur la pile de standards
existant, celle des « identity services ». Il s'agit de services distribués en
réseau qui fonctionnent sous le sceau d'une identité contrôlée par
l'utilisateur, définissant les informations spécifiques (ou attributs) que ces
services peuvent utiliser pour offrir une réponse adaptée et spécifique. Cela
autorise une personnalisation accrue des services, des transactions
intelligentes basées sur les informations d'identité.


SAML_
-----

Le Security Assertion Markup Language (SAML_) d'OASIS_ a été le premier
standard permettant l'échange d'assertion sécurisée.

La plupart des grands groupes offrant des solutions de gestion des accès ont
participé à son élaboration et il a été explicitement conçu pour les relations
business-to-business

SAML_ 1.1 propose sans doute moins de fonctionnalités superflues que d'autres
standards. C'est un protocole propre, simple et concis. Pourtant cette
concision qui le rend relativement facile à implémenter est un handicap quand
il s'agit de l'utiliser dans un autre cadre, business-to-consumer ou
business-to-employee par exemple. Il manque à SAML_ 1.1 les fonctionnalités
concernant la confidentialité, la sécurité et le support des clients mobiles.
L'avènement de SAML_ 2.0 devrait y remédier largement.

SAML_ 2.0 qui vient d'être publié par l'OASIS_ est une forme de convergence
entre SAML_ 1.1, Liberty ID-FF 1.2 et Shibboleth_. La volonté de l'OASIS_ était
de prendre ce qu'il avait de meilleur dans chacun de ces protocoles et de
l'inclure dans un framework unique et cohérent.

Liberty ID-FF
-------------

L'Identity Federation Framework (ID-FF 1.2 et son prédécesseur ID-FF 1.1) a été
élaboré par le consortium `Liberty Alliance`_, fondé mi 2001 par Sun, et
rejoint par plusieurs centaines d'entreprises (France Télécom, Vodafone,
VeriSign, Mastercard, etc.).

ID-FF se base sur SAML_ et le complète permettant des scénarios de déploiement
plus complexes. Il introduit principalement :

* le contrôle de l'utilisateur sur les fédérations opérées ;

* un véritable Single Sign-On et un Single Logout ;

* une véritable anonymisation (pas d'identifiant unique circulant entre les
  fournisseurs de service et le fournisseur d'identité) ;

* un contexte d'authentification (permettant de fournir des informations sur
  l'authentification utilisée mais aussi sur tout ce qui l'entoure, comme la
  procédure d'inscription) ;

* l'échange de metadata.

Conçu pour l'entreprise, en interne et en externe, ID-FF permet de coupler les
exigences d'une authentification forte avec le respect de la vie privée des
usagers, c'est la raison pour laquelle l'ADAÉ_ encourage très fortement son
utilisation au sein des administrations françaises.

Shibboleth_
-----------

Shibboleth_ repose sur SAML_ 2.0. Shibboleth_ est un projet Internet2_
un consortium dirigé par des universités américaines travaillant en partenariat
avec l'industrie et le gouvernement. C'est une implémentation open source
(licence Apache) autorisant le partage inter-institutionnel de ressources web
soumises à un contrôle d'accès.

Shibboleth_ fournit une passerelle standardisé entre les systèmes
d'authentification existant sur les campus et des fournisseurs de ressources en
tous genre. Il inclut l'échange de metadata et des règles de confiance
permettant des accords entre petits groupes de partenaires.Aujourd'hui,
Shibboleth_ est essentiellement utilisé dans l'enseignement supérieur.

WS-Federation_
--------------

Microsoft, IBM, et VeriSign travaillent sur un ensemble de spécifications
(appelées "WS-Security roadmap" ou "WS-\*") pour leur prochaine génération de
plate-forme de Web services.

WS-Federation_ est une de ces spécifications, elle définit un modèle pour la
fédération et les fonctions liées à l'identité.

WS-Federation_ est centré sur l'entreprise, les relations business-to-business
et business-to-employee. L'utilisation de la confidentialité y est optionnelle
et il manque à ce protocole le support du multi-client, ce qui le rend peu
approprié aux environnements business-to-consumer à l'heure actuelle.

Il faut noter qu'en raison de son caractère relativement récent WS-Federation_
n'a pas été testé et déployé de la même façon que les autres protocoles, il
convient donc de l'aborder avec prudence.


Liberty ID-WSF
--------------

L'Identity-based Web Services Framework (ID-WSF) de `Liberty Alliance`_ se
situe au sommet de la pile des protocoles de fédération. La spécificité
d'ID-WSF c'est le service de découverte d'identité qui permet le partage
d'attribut d'identité sous le contrôle de l'utilisateur.

ID-WSF rassemble les éléments suivants :

* partage d'attribut sous permission (l'utilisateur détermine quels attributs
  peuvent publiés et auprès de qui ils peuvent l'être)  ;

* service de découverte d'identité (détermine comment les fournisseurs de
  service apprennent où ils doivent aller chercher les informations d'identité) ;

* service d'interaction (permet aux fournisseurs de service et aux fournisseurs
  d'identité d'interagir avec l'utilisateur en temps réel pour obtenir son
  consentement et les autorisations nécessaires) ;

* support client étendu (permet aux dispositifs clients d'héberger leur propre
  service d'identité ou d'agir comme un fournisseur d'identité) ;

* templates de service d'identité (un mécanisme réutilisable pour construire de
  nouveaux services d'identité pouvant intégrer le framework existant) ;

* règles d'usages (fournit un moyen pour intégrer des règles de confidentialité
  dans l'échange d'attribut).

ID-WSF convient aux déploiements business-to-business et business-to-consumer
où le partage d'attribut de façon confidentielle est central. Un fournisseurs
pourra chercher et découvrir des informations d'identité depuis des services
d'identité distribués que l'utilisateur à autorisé. Les règles relatives à la
publication d'attributs peuvent être définies au préalable ou au moment de la
transaction en demandant les permissions nécessaires à l'utilisateur.


Se procurer et installer Authentic
==================================

Installation sous Debian_ Sarge
+++++++++++++++++++++++++++++++

Pour fonctionner correctement Authentic s'appuie sur :

* Apache_ (1.3 ou 2, Apache2 recommandé) ;

* Lasso_ (0.6.3) ;

* Quixote_ (2.0) ;

* mod_python_ ou SCGI_ (SCGI_ recommandé).

Installation des paquets
------------------------

Il faut ajouter à votre fichier /etc/apt/sources.list la ligne suivante
pour que vous ayez accès au dépôt nécessaire à l'installation d'Authentic ::

 deb http://deb.entrouvert.org/ sarge main

En tant que root tapez ::

 apt-get update
 apt-get install authentic

Tous les paquets nécessaires sont installés.

Si vous ne souhaitez pas modifier votre fichier sources.list vous pouvez
récupérer les paquets nécessaire manuellement et les installer avec la commande
dpkg -i :

* Authentic et Quixote 2.0 sur http://authentic.labs.libre-entreprise.org/ ;

* Lasso sur http://lasso.entrouvert.org.

Configuration d'Apache_
-----------------------

Il faut ensuite configurer Apache_ pour avoir un virtual host Authentic, le
fichier d'example ci-dessous s'appelle authentic et il est installé
par défaut. Il fonctionne (en remplaçant www.example.com par le nom de domaine
que vous avez choisi pour Authentic, nous utiliserons authentic.example.com)
pour Apache2 et SCGI_. Vous le trouverez dans le répertoire
``/etc/apache2/sites-available`` ::

 <VirtualHost *>
   ServerAdmin webmaster@locahost
   ServerName authentic.example.com
   DocumentRoot /usr/share/authentic/web/
   SCGIMount / 127.0.0.1:3002
   <LocationMatch "^/(css|images|js)/.*">
     SCGIHandler off
   </LocationMatch>
   SSLEngine On
   CustomLog /var/log/apache2/authentic-access.log combined
   ErrorLog /var/log/apache2/authentic-error.log
 </VirtualHost>

Pour activer Authentic au sein d'Apache il faut créer un lien vers ce fichier
depuis le répertoire /etc/apache2/sites-available. Utilisez la commande ::

 a2ensite authentic

Il faut également vous assurer qu'Apache_ est configuré pour supporter le SSL,
vérifiez que dans votre fichier /etc/apache2/ports.conf vous avez une ligne ::

 Listen 443

Ajoutez la si elle n'est pas présente. Activez ensuite le support SSL ::

 a2enmod ssl

Ensuite, il s'agit d'activer le module SCGI, s'il ne l'était déjà ::

 a2enmod scgi

Vous pouvez ensuite recharger Apache_ (toujours en root) ::

 /etc/init.d/apache2 reload

Pensez également à modifier votre fichier /etc/hosts le cas échéant. Lorsque
Authentic fonctionne, l'interface d'administration est accessible à l'URL
http://authentic.example.com/admin.

Installation avec une autre distribution Linux
++++++++++++++++++++++++++++++++++++++++++++++

Nous supposons qu'Apache_, SCGI_ ou mod_python_ sont déjà installés. Il faut
ensuite télécharger les sources suivantes et les installer :

* Lasso http://lasso.entrouvert.org ;

* Quixote http://www.mems-exchange.org/software/quixote/ ;

* Authentic http://authentic.labs.libre-entreprise.org/.

Pour installer Authentic, décompressez les sources que vous avez téléchargées
et lancez le script setup.py ::

 tar xzf authentic*.tar.gz
 cd authentic*
 python setup.py install

Il vous faut ensuite configurer correctement Apache_.

Pour lancer authentic tapez en tant que root dans une console ::

 authenticctl.py start
 
.. note:: À noter que pour des raisons de sécurité il est préférable de faire
   lancer Authentic par un utilisateur dédié. Cet utilisateur doit avoir les
   droits d'écriture sur ``/var/lib/authentic``.

Lorsque qu'Authentic fonctionne, l'interface d'administration est accessible à
l'URL http://authentic.example.com/admin.

Installation sous Windows
+++++++++++++++++++++++++

Nous n'avons pas à l'heure actuelle réalisé d'installation d'Authentic sous
Windows. Mais étant donné que tous les composants nécessaires à son utilisation
fonctionne sur ce système d'exploitation, l'installation est envisageable et
nous seront peut-être amenés à la décrire bientôt. N'hésitez pas à nous faire
part de vos tentatives.

Configuration de base d'Authentic
=================================

Création Administrateur
+++++++++++++++++++++++

En principe l'administrateur c'est vous puisque vous lisez cette documentation
qui lui est destinée. Nous vous laissons encore un peu de temps pour
renoncer... trop tard nous considérons désormais que vous êtes l'administrateur
Authentic et nous allons vous aider à créer votre compte.

Il faut en premier lieu aller sur l'interface d'administration
http://authentic.example.com/admin/.

.. figure:: figures/authentic-admin.png

   L'interface d'administration lorsqu'aucun utilisateur n'existe encore.

Cliquez sur l'onglet « Gestion des identités », puis sur le lien « Ajouter une
identité »

.. figure:: figures/authentic-admin-identites-new.png

   Création de la première identité, celle de l'administrateur

Remplissez les champs suivants :

* Nom (saisissez vos Nom et prénom) ;

* Courriel (saisissez votre Courriel) ;

* Rôles (sélectionnez le seul rôle disponible « administrateur ») ;

* Identifiant (Le nom d'utilisateur choisi pour l'administrateur) ;

* Mot de passe (le mot de passe choisi pour l'administrateur).

Le compte est créé vous pouvez maintenant vous identifier en tant
qu'administrateur sur la page d'identification qui vous est présentée.

.. figure:: figures/authentic-login.png

   1ère connexion

Configuration de base du fournisseur d'identité
+++++++++++++++++++++++++++++++++++++++++++++++

Création clés publiques et privées
----------------------------------

Si vous ne possédez pas de clés au format pem, il vous faut les créer. Pour
créer un couple clé publique/clé privée avec OpenSSL_, utilisez les commandes
suivantes ::

 openssl genrsa -out nom-de-la-clé-privé.pem 2048

Cette commande crée la clé privée sous la forme d'un fichier appelé
nom-de-la-clé-privé.pem. ::

 openssl rsa -in nom-de-la-clé-privé-key.pem -pubout\
 -out nom-de-la-clé-publique.pem

Cette commande extrait la clé publique de la clé privée sous la forme d'un
fichier appelé nom-de-la-clé-publique.pem.

Paramètres du fournisseur d'identité
------------------------------------

.. figure:: figures/authentic-admin-settings-liberty_idp.png

   Configuration du fournisseur d'identité

Les deux premiers champs sont remplis automatiquement, ne cherchez pas à les
modifier à moins de savoir réellement ce que vous faites.  Champs :

* Identifiant du fournisseur (un identifiant qui prend nécessairement la forme
  d'une URL) ;

* URL de la racine Liberty (toutes les URL nécessaires à `Liberty Alliance`_
  sont sous cette racine) ;

* Nom de l'organisation (nom de l'organisation qui gère le fournisseur
  d'identité) ;

* Clé privée (clé privée au format pem) ;

* Clé publique (clé publique au format pem) ;

* Domaine commun, pour 'Identity Provider Introduction' (L'identity provider
  introduction est un mécanisme `Liberty Alliance`_ permettant à un
  fournisseur d'identité, pour un nom de domaine particulier, de générer un
  cookie sur la machine de l'utilisateur. C'est utile lorsqu'il y a plusieurs
  fournisseurs d'identités associés à un fournisseur de service : dans le
  cookie on associe les fournisseurs de service d'un domaine, par exemple
  entrouvert.com, au fournisseur d'identité qui a délivré le cookie. Cela permet
  de stipuler au fournisseur de service : « cet utilisateur utilise le
  fournisseur d'identité du domaine ».) ;

* Support pour le proxy ID-FF (L'option proxy ID-FF permet à un fournisseur
  d'identité de jouer le rôle de mandataire actif entre un fournisseur de
  service et le fournisseur d'identité final. Elle n'est utile que lorsque
  plusieurs fournisseurs d'identité sont en relations).

Enregistrement du fichier de metadata
-------------------------------------

Dans l'interface d'administration d'Authentic vous pouvez récupérer le
fichier de metadata. Cela sera utile par la suite lorsqu'il s'agira de
configurer un fournisseur de service. Procédez comme suit :

* cliquez sur l'onglet paramètres ;

* vous voyez un lien «  Metadata du fournisseur d'identités ». Faites un clic
  droit et « enregistrer la cible du lien sous » ;

* choisissez le nom que vous donnez à ce fichier (par exemple
  metadata-authentic.xml) et l'endroit où vous le sauvegardez.

Installation d'un fournisseur de service
========================================

Exemple de fournisseur de service : Candle_
+++++++++++++++++++++++++++++++++++++++++++

Candle_ est fournisseurs de service `Liberty Alliance`_ de démonstration
conçu pour fonctionner avec Authentic. Il est possible que vous souhaitiez
plutôt installer votre propre fournisseur de service, il devrait fonctionner
sans problème s'il est compatible `Liberty Alliance`_. Candle_ présente
l'avantage d'avoir été réalisé par la même équipe qu'Authentic (interface
très proche, attention à ne pas faire de confusion) et de fonctionner
parfaitement.


Installation identique à Authentic
----------------------------------

Pour les utilisateurs de Debian_ Sarge, tapez en tant que root ::

 echo 'deb http://deb.entrouvert.org/ sarge-experimental' \
  >> /etc/apt/sources.list

Cette commande ajoute le répertoire qui contient tous les paquets nécessaires
dans votre fichier sources.list.

Toujours en tant que root tapez ::

 apt-get update
 apt-get install candle

Pour les autres distributions téléchargez les sources sur le site
http://lasso.entrouvert.org/links et suivez exactement les mêmes procédures
que pour l'installation d'Authentic.

Une fois le logiciel installé, l'interface d'administration de Candle_ est
disponible à l'URL http://candle.example.com/admin.

Création de clés publiques et privée
------------------------------------

Si vous ne possédez pas de clés au format pem, il vous faut les créer. Pour
créer un couple clé publique/clé privée avec OpenSSL_, utilisez les
commandes suivantes ::

 openssl genrsa -out nom-de-la-clé-privé.pem 2048

Cette commande crée la clé privée sous la forme d'un fichier appelé
nom-de-la-clé-privé.pem ::

 openssl rsa -in nom-de-la-clé-privé-key.pem -pubout\
 -out nom-de-la-clé-publique.pem

Cette commande extrait la clé publique de la clé privée sous la forme d'un
fichier appelé nom-de-la-clé-publique.pem

Création du fournisseur de service
----------------------------------

Allez sur l'interface d'administration de Candle_
http://candle.example.com/admin. Cliquez sur l'onglet paramètres puis sur le
lien « fournisseur de service »

.. figure:: figures/candle-admin-settings-liberty_sp.png

   Configuration du fournisseur de service Candle

Les deux premiers champs sont remplis automatiquement, ne cherchez pas à les
modifier à moins de savoir réellement ce que vous faites.

Champs :

* Identifiant du fournisseur (un identifiant qui prend nécessairement la
  forme d'une URL) ;

* URL de la racine (toutes les URL nécessaires à `Liberty Alliance`_ se
  trouvent sur cette racine) ;

* Nom de l'organisation (nom de l'organisation qui gère le fournisseur
  d'identité) ;

* Clé privée (clé privée au format pem) ;

* Clé publique (clé publique au format pem) ;

* Domaine commun, pour « Identity Provider Introduction » (L'identity
  provider introduction est un mécanisme `Liberty Alliance`_ permettant à un
  fournisseur d'identité, pour un nom de domaine particulier, de générer un
  cookie sur la machine de l'utilisateur. C'est utile lorsqu'il y a plusieurs
  fournisseurs d'identités associés à un fournisseur de service : dans le
  cookie on associe les fournisseurs de service d'un domaine, au fournisseur
  d'identité qui a délivré le cookie. Cela permet de stipuler au fournisseur de
  service : « cet utilisateur utilise le fournisseur d'identité du domaine
  ».).

Enregistrement du fichier de metadata
-------------------------------------

Dans l'interface d'administration de Candle_ vous pouvez récupérer le
fichier de metadata. Cela sera utile par la suite lorsqu'il s'agira de
déclarer Candle_ fournisseur de service sur . Procédez comme suit :

* cliquez sur l'onglet paramètres ;

* vous voyez un lien «  Metadata du fournisseur de service ». Faites un clic
  droit et « enregistrer la cible du lien sous » ;

* choisissez le nom que vous donnez à ce fichier (par exemple
  metadata-candle.xml) et l'endroit ou vous le sauvegardez.

Déclarer Authentic comme fournisseur d'identité sur Candle_
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Sur l'interface d'administration de Candle_, cliquez sur l'onglet
paramètres, puis sur le lien « fournisseurs d'identités ». Cliquez encore
sur  « nouveau »

.. figure:: figures/candle-admin-settings-liberty_idp-new.png

   Déclarer Authentic comme fournisseur d'identité de Candle

Compléter les champs suivants :

* Metadata (le fichier de metadata d'Authentic) ;

* Clé publique (la clé publique d'Authentic) ;

* Chaîne de certification (certificat contenant toute la chaîne
  d'authentification jusqu'au root CA).

Déclarer Candle_ comme fournisseur de service sur Authentic
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Il nous faut déclarer Candle_ en tant que fournisseur de service lié au
fournisseur d'identité Authentic. Pour cela allez sur l'interface
d'administration d'Authentic :

 * Cliquez sur l'onglet paramètres ;

 * Cliquez sur le lien « fournisseurs Liberty » ;

 * Cliquez sur le lien « nouveau ».

.. figure:: figures/authentic-admin-settings-liberty_providers-new.png

   Nouveau fournisseur de service déclaré sur Authentic

Il faut compléter les champs suivants :

* Metadata (le fichier de metadata de Candle_) ;

* Clé publique (la clé publique de Candle_) ;

* Chaîne de certification (certificat contenant toute la chaîne
  d'authentification jusqu'au root CA).

* Autoriser le SSO initié par l'IdP (Permet que le Single Sign-On soit initié
  indifféremment par le fournisseur de service ou par le fournisseur
  d'identité).

Exemple de fournisseur de service : Spip
++++++++++++++++++++++++++++++++++++++++

Spip est outil collaboratif d'aide à la réalisation de sites web éditoriaux.

Installation identique à Authentic avec le paquet ou le sources sur notre site
------------------------------------------------------------------------------

Création clés publiques et privées
----------------------------------

Si vous ne possédez pas de clés au format pem, il vous faut les créer. Pour
créer un couple clé publique/clé privée avec OpenSSL_, utilisez les
commandes suivantes ::

 openssl genrsa -out nom-de-la-clé-privé.pem 2048

Cette commande crée la clé privée sous la forme d'un fichier appelé
nom-de-la-clé-privé.pem ::

 openssl rsa -in nom-de-la-clé-privé-key.pem -pubout\
 -out nom-de-la-clé-publique.pem

Cette commande extrait la clé publique de la clé privée sous la forme d'un
fichier appelé nom-de-la-clé-publique.pem

Création du fournisseur de service
----------------------------------

Enregistrement du fichier de metadata
-------------------------------------

Déclarer Authentic comme fournisseur d'identité sur spip
++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Déclarer Spip comme fournisseur de service sur Authentic
++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Utilisation et paramétrage d'Authentic
======================================

Créer et modifier des utilisateurs
++++++++++++++++++++++++++++++++++

Il y a quatre moyens d'ajouter de nouveaux utilisateurs : 

* créer leurs identités une par une ;

* en générer un grand nombre automatiquement par le biais d'un fichier au format
  CSV ;

* récupérer les informations contenues dans un annuaire LDAP ;

* laisser les utilisateurs créer eux-mêmes leurs identités.

Lorsque vous cliquez sur l'onglet « Gestion des identités » vous obtenez la
liste des utilisateurs enregistrés.

.. figure:: figures/authentic-admin-identites.png

	Liste des utilisateurs enregistrés

Pour chaque utilisateur figure en face de son nom 4 icônes permettant
d'effectuer les tâches suivantes concernant son compte : voir, modifier,
supprimer, afficher les logs.

Ajouter un seul utilisateur
---------------------------

Pour créer un nouvel utilisateur unique, cliquez sur l'onglet « Gestion des
identités » puis sur le lien « Ajouter une identité ».

.. figure:: figures/authentic-admin-identites-new-user.png

   Ajouter une identité

Remplissez les champs suivants :

* Nom (saisissez les Nom et prénom de l'utilisateur) ;

* Courriel (saisissez le courriel de l'utilisateur) ;

* sélectionnez le rôle « administrateur » si vous voulez créer un autre
  administrateur ou laissez le champ vide pour créer un utilisateur normal (le
  bouton ajouter un rôle n'est utile que lorsque des rôles intermédiaires
  entre « utilisateur » et « administrateur » existent, ce n'est pas le cas
  ici) ;

* Identifiant (Le nom d'utilisateur choisi pour l'utilisateur) ;

* Mot de passe (le mot de passe choisi pour l'utilisateur).

Importer les identités depuis un fichier CSV
--------------------------------------------

Plutôt que de créer un grand nombre d'utilisateurs un par un, il est
possible d'en générer automatiquement un grand nombre grâce à un fichier CSV
organisé comme suit ::

 identifiant;mot de passe;nom;courriel

Pour cela cliquez sur l'onglet « Gestion des identités » puis sur le lien «
Importer des identités depuis un fichier CSV ». Cliquez sur le bouton
parcourir et sélectionnez le fichier CSV que vous avez préparé.

Utiliser un annuaire LDAP
--------------------------

Vous pouvez utiliser la base utilisateur de votre annuaire LDAP (ou LDAPs) 
comme source de données : tous vos utilisateurs LDAP auront alors une identité
sur Authentic.
Les utilisateurs doivent avoir un accès direct à l'annuaire LDAP. Après la 
déclaration de l'annuaire LDAP, un utilisateur doit être désigné comme 
administrateur (dans l'interface d'Authentic), sinon l'ensemble des utilisateurs 
aura accès à l'interface d'amdinistration d'Authentic.

Cliquez sur l'onglet « Paramètres », puis sur le lien stockage des
identités, sélectionnez annuaire LDAP dans la liste « Origine des données
», cliquez sur valider.

.. figure:: figures/authentic-admin-settings-identity_storage.png

   Configuration de l'annuaire LDAP

Saisissez les différents paramètres LDAP :

* URL (URL du serveur LDAP ou LDAPs) ;

* Racine (Racine de l'arbre LDAP) ;

* Classe des objets (Classe à laquelle appartiennent les objets utilisateurs). Pour Active Directory cela correspond à la
  valeur  « user ».

* Attribut « identifiant » des objets (Champ désignant l'identifiant (ou nom 
  d'utilisateur) dans l'annuaire LDAP). Pour Active Directory cela correspond à
  la valeur « sAMAccountName ».

* LDAP Object User Name Attribute (Champ désignant le nom d'utilisateur dans
  l'annuaire LDAP). Pour Active Directory cela correspond à la valeur
  « UserName ».

* LDAP Object Email Attribute (Champ désignant l'email dans l'annuaire LDAP);
  Pour Active Directory cela correspond à la valeur « mail ».

* Massive LDAP Directory (Sélectionnez cette option pour améliorer les
  peformances d'un annuaire LDAP volumineux).

Attention, l'utilisateur doit pouvoir faire un "bind" sur l'annuaire, avec son
identifiant et son mot de passe. L'utilisateur, qui aura un rôle d'admin dans
Authentic, doit pouvoir lister les objets LDAP.

Laisser les utilisateurs créer leurs identités
----------------------------------------------

Au lieu de réserver la création d'identité à l'administrateur, il est
possible d'activer une option permettant à tout utilisateur n'ayant pas
encore de compte de le créer depuis la page de connexion. Lorsque cette
option est sélectionnée, un lien supplémentaire « Créer un nouveau compte »
apparaît sur l'écran de connexion initial, sous le bouton « s'identifier ».
Ce lien permet à l'utilisateur de se retrouver sur un formulaire de création
d'identité identique à celui de l'administrateur.

Pour activer cette option, cliquez sur l'onglet « Paramètres », puis sur le
lien « Option pour les identités ». Dans la boite « Création des identités
», sélectionnez « inscription par l'utilisateur » puis validez.

Modifier les données d'un utilisateur
-------------------------------------

Pour modifier les données concernant un utilisateur existant, cliquez sur
l'onglet « Gestion des identités » puis sur la deuxième icône en face de son
nom. Vous pouvez alors changer ce qui doit l'être.

Paramètres des identités
++++++++++++++++++++++++

Un certain nombre d'options sont paramétrables concernant la façon dont les
identités sont créées. Elles sont accessibles sur l'interface
d'administration dans l'onglet « Paramètres » sous la rubrique « identités ».

Options pour les identités
--------------------------

En suivant le lien « Options pour les identités » on peut paramétrer quatre
éléments :

* Définir si les utilisateurs peuvent créer eux-mêmes des comptes ou si seul
  l'administrateur a le droit de le faire. Lorsque cette option est cochée un
  lien « créer un nouveau compte » apparaît sur la page d'identification
  permettant à l'utilisateur de créer seul un compte et de recevoir son mot de
  passe par mail.

* Définir si l'administrateur doit recevoir un message lors de la création d'une
  identité par un utilisateur. Il le sera si vous cochez « notifier les
  administrateurs lors d'inscriptions ».

* Définir si l'adresse mail doit-être utilisée comme identifiant. Lorsque
  cette option est cochée l'adresse mail de l'utilisateur sera automatiquement
  utilisée comme identifiant, il n'y a plus de possibilité pour l'utilisateur
  de choisir cet identifiant.

* Écrire un courriel de bienvenue. Il s'agit du message qui sera
  automatiquement envoyé à tous les nouveaux utilisateurs. Le texte est libre
  mais gagne beaucoup d'utilité à comporter le mot de passe de l'utilisateur.  À
  l'endroit où vous voulez le voir figurer dans le texte écrivez simplement
  « [password] » (sans les guillemets).  Il est également possible d'insérer
  l'identifiant avec « [username] » et le nom du serveur avec « [hostname] ».

Stockage des identités
----------------------

En suivant le lien « stockage des identités » vous avez le choix entre deux
types de stockage :

* Stockage par défaut (fichiers) (stocke chaque identités dans un fichier séparé
  dans le répertoire ``/var/lib/authentic/``).

* Annuaire LDAP (permet d'utiliser un annuaire LDAP comme source des identités,
  cf. `Utiliser un annuaire LDAP`_, dans la partie créer et modifier des
  utilisateurs) ;

Mots de passe
-------------

En suivant le lien « Mots de passe », vous pouvez paramétrer les éléments
suivants :

* permettre à l'utilisateur de changer son mot de passe ;

* choisir de générer automatiquement le mot de passe initial (choix par
  défaut) ou de laisser l'utilisateur le choisir ;

* déterminer la façon dont un utilisateur qui perd son mot de passe pourra
  le récupérer (écrire à l'administrateur, recevoir automatiquement le mot de
  passe par courriel, répondre à une question qu'il aura choisie et pour
  laquelle il aura donné la bonne réponse au moment de l'inscription avant de
  recevoir le mot de passe par courriel) ;

* définir la taille maximale et minimale des mots de passe (une valeur 0
  indique qu'il n'y a pas de limite de taille).

Paramètres de personnalisation
++++++++++++++++++++++++++++++

Un certain nombre d'options sont disponibles pour personnaliser Authentic.
Elles sont accessibles sur l'interface d'administration dans l'onglet «
Paramètres » sous la rubrique « personnalisation ».

Langue
------

En suivant le lien « Langue » vous pouvez définir la langue qui sera utilisée.

Thèmes
------

En suivant le lien « Thème », vous pouvez choisir entre différents thèmes
graphiques qui vont s'appliquer à l'interface utilisateur. L'interface
d'administration, elle, demeure invariable et correspond au thème « Défault ».
Vous trouverez plus de détail dans la partie paramètres avancés,
section `Personnaliser le thème`_.

Squelettes
----------

En suivant le lien « Squelette », vous accédez au modèle utilisé pour
l'affichage des pages et vous pouvez le modifier. Le bouton « Restaurer le
squelette par défaut » permet de rétablir le squelette original.
La syntaxe des squelettes est détaillée dans la partie paramètres avancés,
section `Personnaliser le squelette`_.

Pages publiques
---------------

Afin de pousser la personnalisation au maximum, vous pouvez 
modifier individuellement l'affichage de chacune de ces pages :

* Gestion du compte

* Inscription

* Inscription terminée

* Modification du mot de passe

* Connexion

* Mot de passe perdu

* Mot de passe perdu (envoyé)

* Mise à jour des données personnelles

La modification de ces pages est expliquée dans la partie paramètres avancés,
section `Personnaliser les pages publiques`_.
 
Courriel
--------

En suivant le lien « Courriel », vous pouvez définir trois choses :

* le serveur SMTP utilisé par Authentic pour l'envoi des mail ;

* l'émetteur des courriels (qui apparaîtra dans le champ from des messages
  envoyés aux utilisateurs) ;

* adresse pour le « Reply-To » (si vous souhaitez que l'adresse à laquelle
  les utilisateurs vont répondre soit différente de l'adresse de l'émetteur).

Bouton d'annulation
-------------------

En suivant le lien « Écran de connexion », vous pouvez ajouter à cet écran un
bouton annulation. Ce bouton permet d'être redirigé sur le fournisseur de
service d'où vient l'utilisateur. C'est utile lorsque l'utilisateur se retrouve
sur l'écran de connexion par erreur.

Logs
++++

L'onglet « Logs » présente certaines traces laissées par les users sur le
serveur :

.. figure:: figures/authentic-admin-logger.png

   Liste des traces laissées par les utilisateurs

Les différentes traces enregistrées sont les suivantes :

* authentication : une authentification a été réussie ;

* authentication failure : une authentification a échoué ;

* changed password : un mot de passe a été modifié ;

* changing password page : Il y a eu un accès à la page permettant de changer
  de mot de passe ;

* changing password page (had_errors) : sur la page permettant de changer
  de mot de passe, l'utilisateur a provoqué des erreurs en saisissant un
  nouveau mot de passe identique à l'ancien ou en ne saisissant pas deux fois
  à l'identique sont nouveau mot de passe ;

* created new identity (identifiant de l'utilisateur) : une
  identité a été créée par l'administrateur ;

* deleted identity (identifiant de l'utilisateur) : un utilisateur a été
  effacé ;

* fedterm to (identifiant du fournisseur) : le fournisseur de service à mis fin
  à une fédération ;

* internal server error : le serveur a rencontré une erreur interne ;

* login page : il y a eu un accès à la page de login ;

* login page, cancel : le bouton annuler sur la page de login a été utilisé ;

* login page, proxying to (identifiant du fournisseur) : l'utilisateur a
  demande à être redirigé vers un autre fournisseur d'identité pour lequel
  Authentic sert de proxy ;

* lost password page : il y a eu un accès à la page permettant de récupérer un
  mot de passe perdu ;

* lost password -> email password (identifiant de l'utilisateur) : mot de passe
  envoyé à l'utilisateur par mail ;

* logout : une déconnection simple a été faite ;

* SLO from (identifiant du fournisseur) : il y a un une déconnection
  générale initiée par le fournisseur de service en utilisant le protocole http
  (redirect) ;

* SLO/SOAP from (identifiant du fournisseur) : il y a un une déconnection
  générale initiée par le fournisseur de service en utilisant le protocole
  SOAP ;

* SSO from (identifiant de l'utilisateur) : il y a eu une connection avec une
  identité fédérée ;

* SSO to (identifiant du fournisseur) : il y a eu une connection avec une 
  identité fédérée initiée depuis le fournisseur de service ;

* updated identity : l'utilisateur a modifié des données de son identité ;

* updating personal information : l'utilisateur a accéder à la page de
  modification des données personnelles ;

* user created new identity (identifiant de l'utilisateur) : un utilisateur a
  créé une identité.

Paramètres de debug
+++++++++++++++++++

Pour faciliter le débogage, des options sont accessibles sur l'interface
d'administration dans l'onglet « Paramètres » sous la rubrique « Debug ».

Options de Debug
----------------

En suivant le lien « Options de Debug » vous pouvez paramétrer :

* Activer le panneau de debug (l'activation d'un onglet « Debug » dont le
  fonctionnement est expliqué au paragraphe suivant) ;

* Courriel pour les tracebacks (l'adresse de messagerie à laquelle doivent être
  envoyées les traces des erreurs rencontrées) ;

* Affichage des exceptions (l'affichage ou non des erreurs et le format de cet
  affichage).

Panneau de Debug
----------------

Si l'option « Activation du panneau de Debug » a été retenue, un onglet
supplémentaire apparaît dans l'interface d'administration, l'onglet « Debug
». Cliquez sur cet onglet, puis sur le lien « Sessions ».

.. figure:: figures/authentic-admin-debug-sessions.png

   Liste des sessions

Vous obtenez la liste des dernières sessions avec pour chacune d'entre elles :

* l'adresse de connection ;

* l'identifiant de l'utilisateur ;

* l'heure de connection ;

* l'heure du dernier accès.

Déclarer un bug Authentic
-------------------------

Vous pouvez déclarer un bug ou faire part d'une fonctionnalités que vous
souhaiteriez voir ajouter à Authentic sur http://bugs.entrouvert.org.

Paramètres avancés
==================

Il est possible de personnaliser totalement les pages
publiques d'Authentic en jouant avec les paramètres suivants dans cet ordre
(c'est important) : Thème, squelette, pages publiques.

Personnaliser le thème
++++++++++++++++++++++

Le thème établit le style général des pages publiques. Un certain nombre de
thèmes sont disponibles et définissent (grâce à l'utilisation de feuilles de 
style) les éléments de base de la mise en forme des pages publiques. Vous pouvez
définir votre propre thème (avec votre bandeau, votre logo...) si vous avez les
compétences recquises en matières de CSS.

Un thème se compose uniquement de deux fichiers : desc.xml et
nom-de-la-feuille-de-style.css. Le ficher desc.xml est un fichier XML comprenant
quelques informations de bases sur le thème : son nom, sa version, son
étiquette, sa description, son auteur. La feuille de style définit les
différentes propriétés appliquées aux éléments de chaque pages. Ces deux
fichiers doivent être placés dans un répertoire unique et situé sous
/usr/share/authentic/themes/. Une fois le répertoire et les deux fichiers créés
le thème est disponible dans l'interface d'administration, vous pouvez
l'appliquer.

Personnaliser le squelette
++++++++++++++++++++++++++

Le squelette définit l'agencement de toutes les pages publiques à l'intérieur
d'un thème particulier.
C'est-à-dire qu'au sein du thème choisi, il est encore possible d'agir sur la
présentation de toutes les pages publiques en modifiant le squelette utilisé.
Les squelettes sont de simples fichiers texte qui contiennent (entre autres
choses)un certain nombre de variables, écrites entre paranthèses carrés. Ces
variables sont remplacés dans la page publique, par la valeur qui leur a été
attribuée. Voici les variables à connaître pour faire des modifications dans le
squelette :

* [page_title] : le titre de la page affiché dans la barre de titre s'il est
  correctement positionné dans une balise <title> ;
* [css] : le nom du fichier contenant la feuille de style ;
* [script] : javascripts utilisés par Authentic pour certaines fonctionnalités
  comme le tri des listes ;
* [onload] : instructions en javascript déclenchées lors d'un évènement. Cette
  variable a vocation à être placée comme valeur de l'attribut onload dans la
  balise <body> ;
* [body_class] : cette variable a vocation à être placée comme valeur de
  l'attribut class de la balise <body>. Cette valeur est utilisée par la feuille
  de style ;
* [title] : titre de la page affiché en haut de celle-ci ;
* [org_name] : nom de l'organisation défini lors de la création du fournisseur
  d'identité ;
* [prelude] : variable vide pour l'instant ;
* [breadcrumb] : variable vide pour l'instant ;
* [body] : contenu principal de la page, habituellement situé entre le titre et
  le pied de page ;

Il est possible de tester qu'une variable est remplie en utilisant la syntaxe
suivante : [if-any nom-de-la-variable]...[end]

Personnaliser les pages publiques
+++++++++++++++++++++++++++++++++

Pour aller encore plus loin dans la personnalisation du site, il est possible de
definir pour chaque pages des variations à l'intérieur du thème et du squelette
en vigueur. Comme pour le squelette on recourt à des variables écrites entre des
paranthèses carrés.

Gestion du compte
-----------------

Cette page est la page sur laquelle se trouve l'utilisateur immédiatement après
s'être identifier. Les variables disponibles pour cette page sont :

* identity_label (le nom de l'utilisateur) ;
* idp_sso_list (la liste des fournisseurs de services sur lesquels
  l'utilisateur peut se connecter) ;
* federations_list (la liste des fédérations actives).

Inscription
-----------

Cette page est celle présentée à l'utilisateur lorsqu'il créé son identité
(lorsque l'option permettant aux utilisateurs de créer leurs identités à été
sélectionnée). Une seule variable disponible pour cette page :

* register_form (le formulaire à remplir pour créer l'identité).

Inscription terminée
--------------------

Cette page est celle présentée à l'utilisateur lorsqu'il a validé son
inscription. Elle ne contient pas de variables.

Modification du mot de passe
----------------------------

Cette page est celle présentée à l'utilisateur lorsqu'il veut modifier son mot
de passe. Une seule variable disponible pour cette page :

* change_password_form (le formulaire à remplir pour changer son mot de passe) ;

Connexion
---------

Cette page est la page sur laquelle l'utilisateur doit s'identifier. Les
variables disponibles pour cette page sont :

* login_form (le formulaire permettant de saisir identifiant et mot de passe) ;
* authentication_failure (le message indiquant l'échec de l'identification).

Mot de passe perdu
------------------

Cette page est celle présentée à l'utilisateur lorsqu'il indique qu'il a perdu
son mot de passe. Les variables disponibles pour cette page sont :

* lost_password_form (le formulaire dans lequel l'utilisateur saisit son
  identifiant) ;
* behaviour : variable qui n'est pas destiné à être affiché mais qui contient le
  type de comportement adopté en cas de perte du mot de passe (rien, envoi par
  mail, question poséee). Cette variable doit être utilisée avec des conditions
  de ce type :

  * [is behaviour "email_reminder"] Vous allez recevoir un email [end] ;
  * [is behaviour "dumb_question"] Une question va vous être posée [end].

Mot de passe perdu question
---------------------------
Cette page est celle présentée à l'utilisateur lorsqu'il indique qu'il a perdu
sont mot de passe et qu'une question lui est posée pour le récupérer.
Une seule variable disponible pour cette page :

* lost_password_question_form (le formulaire posant la question et enregistrant
  la réponse).

Mot de passe perdu (envoyé)
---------------------------

Cette page est celle présentée à l'utilisateur lorsque son mot de passe lui a
été envoyé. Elle ne contient pas de variables.

Mise à jour des données personnelles
------------------------------------

Cette page est celle présentée à l'utilisateur lorsqu'il met à jour les données
concernant son identité. Une seule variable disponible pour cette page :

* info_form (le formulaire affichant les données de l'utilisateur et
  enregistrant les modifications).

Licences
========

Authentic, Candle_ et Lasso_ sont publiés sous la `licence GNU/GPL`_.

.. _Lasso: http://lasso.entrouvert.org/
.. _`Liberty Alliance`: http://projectliberty.org/
.. _`licence GNU/GPL`: http://www.gnu.org/copyleft/gpl.html
.. _SAML: http://www.oasis-open.org/specs/index.php#samlv2.0
.. _Shibboleth: http://shibboleth.internet2.edu/
.. _OASIS: http://www.oasis-open.org/
.. _ADAÉ: http://www.adae.gouv.fr/
.. _Internet2: http://internet2.edu/
.. _WS-Federation: http://msdn.microsoft.com/webservices/understanding/gxa/default.aspx?pull=/library/en-us/dnglobspec/html/ws-federation.asp
.. _Debian: http://www.debian.org/
.. _Apache: http://www.apache.org/
.. _Quixote: http://www.mems-exchange.org/software/quixote/
.. _mod_python: http://www.modpython.org/
.. _SCGI: http://www.mems-exchange.org/software/scgi/
.. _OpenSSL: http://www.openssl.org
.. _Candle: http://candle.labs.libre-entreprise.org/
