To use authentic with SSL authentication your must configure Apache to do so.
Here is an example of an Apache virtual host to use SSL client authentication:

<VirtualHost 127.0.0.1:80>
ServerName authentic.localhost
Include /home/bdauvergne/wd/authentic-apache2.conf
</VirtualHost>

<VirtualHost 127.0.0.1:443>
ServerName authentic.localhost
LogLevel debug
Include /home/bdauvergne/wd/authentic-apache2.conf
SSLEngine on
SSLCertificateFile /etc/apache2/certificates/localhost-wildcard.pem
SSLVerifyClient none

<Location /login_ssl>
SSLVerifyClient optional_no_ca
SSLOptions +StdEnvVars +ExportCertData
</Location>

<Location "/associate_certificate">
SSLVerifyClient optional_no_ca
SSLOptions +StdEnvVars +ExportCertData
</Location>
</VirtualHost>

As you can see we do not force SSL client verification on the full site, so as
to permit other kind of authentications without spurious dialog asking you for
a certificate.  We could use «SSLVerifyClient optional» on the full site but it
would ask even user logging in with a password for a certificate (they can
still select the cancel button, bit it stil waste their time). The other
problem with «optional» and «require» value for SSLVerifyClient is that they do
not work with certificate coming from an unknown CA or self-signed. These kind
of certificate are still a strong authentication for any user, stronger than a
shared secret sent in the clear.

A current problem is that authentic do not generate a new cookie when
authenticating with ssl and do not set it with the 'secure' flag that would
prevent the navigator to release upon a not secured plain HTTP connection.

Another problem is that we do not use the SSL session_id as a persistent cookie
instead of a simple HTTP cookie, so an attacker can still do a MITM attack
after the authentication. If the user is stupid enough to accet an hijacked SSL
session and a clik throught the warning of the navigator, then the attacker can
steal the HTTP secure cookie.
