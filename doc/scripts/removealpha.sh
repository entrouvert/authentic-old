#! /bin/sh

size=$(identify $1 | cut -d ' ' -f 3)
composite -compose atop $1 -size $size xc:white $2

